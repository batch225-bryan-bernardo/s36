// Dependencies setup

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();

const taskRoute = require("./routes/taskRoute");

// server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Database connection
// Connection to MongoDB Atlas

mongoose.connect(`mongodb+srv://bernardo_bryan2396:${process.env.PASSWORD}@cluster0.wizmpvw.mongodb.net/MRC?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

// Setup notification for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB Atlas"));


// Add the task routes
// Allows all the task routes created in the "taskRoutes.js"  file to use "/tasks" route

app.use("/tasks", taskRoute);

// Server listener
app.listen(port, () => console.log(`You are connected to port ${port}`));