// Contains all the endpoints for our application

// We need to use express' Router() function to achieve this

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system.
// Allows acces to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router();

const taskController = require("../controllers/taskController");
const task = require("../models/task");

/* 
    Syntax : localhost:4000/tasks
*/
// [Section] Routes
router.get("/getinfo", (req, res) => {

    taskController.getAllTasks().then(
        resultFromController => res.send(resultFromController));

});

// Route to create new task

router.post("/create", (req, res) => {

    // If information will be coming from client side commonly from "forms", the data can be accessed from the request "body" property.
    taskController.createTask(req.body).then(resultFromController => {
        res.send(resultFromController)
    })

})

// Route to delete a task
// The colon (:) is ano identifier that helps create a dynamic route which allows us to supply information in the URL.
// The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wild card where you can put any value, it then creates a  link between "id" paramter in the URL and the value provided in the URL.

/* 
    Ex.
    localhost:3000/tasks/delete/12345
    The "12345" is assingned to the "id" parameter in the URL.
*/

router.delete("/delete/:id", (req, res) => {
    // URL parameter values are accessed via the request object's "params" property
    // The property name of this object will match the given URL parameter name
    // In this case "id" is the name of the parameter
    // If information will be coming from the URL, the data can be accessed from the request "params" property
    taskController.deleteTask(req.params.id).then((resultFromController) => {
        res.send(resultFromController);
    })


})

// Route to update a tasl

router.put("/update/:id", (req, res) => {

    taskController.updateTask(req.params.id, req.body).then((resultFromController) => {

        res.send(resultFromController);

    })
})


// ACTIVITY S37 (JANUARY 17, 2023)


router.get("/:id", (req, res) => {

    taskController.getTask(req.params.id).then((resultFromController) => {

        res.send(resultFromController);

    })

})

router.put("/:id", (req, res) => {

    taskController.updateStatus(req.params.id, req.body).then((resultFromController) => {

        res.send(resultFromController);

    })
})

// Use "module.exports" the router objects to use in the "app.js"

module.exports = router;